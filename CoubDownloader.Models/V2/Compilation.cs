﻿using System;

namespace CoubDownloader.Models.V2
{
    public class Compilation : EntityBase
    {
        public TimeSpan Duration { get; set; }
        public StorageProperties StorageProperties { get; set; }
        public DateTime CreatedAt { get; set; }
        public CompilationStatus Status { get; set; }
    }
}