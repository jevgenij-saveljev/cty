﻿using System;

namespace CoubDownloader.Models.V2
{
    public class Coub : EntityBase
    {
        public string Permalink { get; set; }
        public string StorageLink { get; set; }
        public DateTime CreatedAt { get; set; }
        public TimeSpan Duration { get; set; }

        public StorageProperties StorageProperties { get; set; }
    }
}