﻿using Microsoft.EntityFrameworkCore;

namespace CoubDownloader.Models.V2
{
    [Owned]
    public class StorageProperties
    {
        public string ContainerName { get; set; }
        public string FileName { get; set; }
    }
}