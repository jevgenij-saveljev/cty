﻿namespace CoubDownloader.Models.V2
{
    public class CompilationCoub: EntityBase
    {
        public long CoubId { get; set; }
        public long CompilationId { get; set; }
        public int Order { get; set; }

        public Coub Coub { get; set; }
        public Compilation Compilation { get; set; }
    }
}