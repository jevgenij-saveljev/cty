﻿namespace CoubDownloader.Models.V2
{
    public enum CompilationStatus
    {
        Fulfilment = 1,
        Processing = 2,
        Done = 3,
        ErrorWhileProcessing = -1
    }
}