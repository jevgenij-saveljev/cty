﻿namespace CoubDownloader.Models
{
    public class CoubEntry : EntityBase
    {
        public string Link { get; set; }

        public string Name { get; set; }

        public CoubCompilation CoubCompilation { get; set; }

        public int Order { get; set; }
    }
}

