﻿using CoubSharp.Model;
using System.Collections.Generic;

namespace CoubDownloader.Models
{
    public class CoubList
    {
        public IEnumerable<Coub> coubs { get; set; }
        public int page { get; set; }
        public int total_pages { get; set; }
        public int per_page { get; set; }


    }
}
