﻿using CoubSharp.Model;
using System;

namespace CoubDownloader.Models
{
    public class FileSystemCoub
    {
        public Coub Coub { get; set; }
        public string Path { get; set; }
        public TimeSpan Length { get; set; }
    }
}
