﻿using System;
using System.Collections.Generic;

namespace CoubDownloader.Models
{
    public class CoubCompilation : EntityBase
    {
        public DateTime CreatedAt { get; set; }

        public List<CoubEntry> Coubs { get; set; }
    }
}
