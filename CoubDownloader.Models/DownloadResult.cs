﻿using CoubSharp.Model;

namespace CoubDownloader.Models
{
    public struct DownloadResult
    {
        public DownloadResult(Coub coub, string videoPath, string audioPath)
        {
            this.Coub = coub;
            this.VideoPath = videoPath;
            this.AudioPath = audioPath;
        }

        public Coub Coub { get; }
        public string VideoPath { get; }
        public string AudioPath { get; }
    }
}
