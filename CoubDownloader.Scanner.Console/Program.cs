﻿using System;
using System.Threading.Tasks;
using CoubDownloader.ScannerStrategies;

namespace CoubDownloader.Scanner.Console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var scanner = new LikesFeedStrategy();
            await scanner.Scan();
            System.Console.WriteLine("Hello World!");
        }
    }
}