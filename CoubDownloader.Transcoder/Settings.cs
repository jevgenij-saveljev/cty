﻿using System;

namespace CoubDownloader.Transcoder
{
    public class Settings
    {
        public static string StorageAccount =>
            Environment.GetEnvironmentVariable("STORAGE_ACCOUNT", EnvironmentVariableTarget.Process);
        
        public static string CoubToken =>
            Environment.GetEnvironmentVariable("COUB_TOKEN", EnvironmentVariableTarget.Process);
        
        public static string CoubPermalink =>
            Environment.GetEnvironmentVariable("COUB_PERMALINK", EnvironmentVariableTarget.Process);
    }
}