﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using CoubDownloader.Models.V2;
using CoubDownloader.Persistance;
using CoubSharp.Model;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CoubDownloader.Transcoder
{
    class Program
    {
        private static BlobContainerClient _client;

        static async Task Main(string[] args)
        {
            PrintDebug();
            Setup();


            Console.WriteLine($"get coub: {Settings.CoubPermalink}");
            var coub = await CoubApiService.GetCoubAsync(Settings.CoubPermalink);
            Console.WriteLine(JsonConvert.SerializeObject(coub, Formatting.Indented));
            Console.WriteLine($"Download coub to: {SourceDirectory}");
            var result = await CoubApiService.DownloadCoubAsync(coub, SourceDirectory );
            var merged = CoubApiService.MergeCoubAudioAndVideo(result, OutputDirecotry);

            var fName = Path.GetFileName(merged.Path);


            BlobContentInfo blobContainerInfo = null;
            using (var fs = new FileStream(merged.Path, FileMode.Open))
            {
                _client.DeleteBlobIfExists(fName);
                var res = _client.UploadBlob(fName, fs);
            }

            var newCoub = new CoubDownloader.Models.V2.Coub()
            {
                Duration = merged.Length,
                Permalink = Settings.CoubPermalink,
                CreatedAt = DateTime.Now,
                StorageLink = fName
            };
            
            var ctx = new CoubAppContext();
            var coubDb = ctx.Coubs.FirstOrDefault(x => x.Permalink == newCoub.Permalink);
            
            if (coubDb != null)
            {
                coubDb.Duration = newCoub.Duration;
                coubDb.Permalink = newCoub.Permalink;
                coubDb.CreatedAt = newCoub.CreatedAt;
                coubDb.StorageLink = newCoub.StorageLink;
            }
            else
            {
                ctx.Coubs.Add(newCoub);
            }

            ctx.SaveChanges();
        }

        private static CoubApiService CoubApiService { get; set; }
        private static string SourceDirectory { get; set; }
        private static string CurrentDiectory { get; set; }
        private static string OutputDirecotry { get; set; }

        private static void PrintDebug()
        {
            Console.WriteLine(Settings.StorageAccount);
            Console.WriteLine(Settings.CoubToken);
            Console.WriteLine(Settings.CoubPermalink);
        }

        private static void Setup()
        {
            CoubApiService = new CoubApiService(Settings.CoubToken);

            CurrentDiectory = Directory.GetCurrentDirectory();
            SourceDirectory = Path.Combine(CurrentDiectory, "sources");
            Directory.CreateDirectory(SourceDirectory);

            OutputDirecotry = Path.Combine(CurrentDiectory, "output");
            Directory.CreateDirectory(OutputDirecotry);
            
            InitStorage();
        }

        private static void InitStorage()
        {
            var blobContainerName = "coubs";
            _client = new BlobContainerClient(Settings.StorageAccount, blobContainerName);
            _client.CreateIfNotExists();
        }
    }
}