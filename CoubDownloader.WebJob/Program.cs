﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CoubDownloader.FileStorage;
using CoubDownloader.Messaging;
using CoubDownloader.Messaging.DataTypes;
using CoubDownloader.Persistance;
using CoubDownloader.ScannerStrategies;
using CoubDownloader.WebJob.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace CoubDownloader.WebJob
{
    class Program
    {
        static Program()
        {
            StorageAccount = CloudStorageAccount.Parse(SettingsProvider.StorageAccount);
            QueueClient = StorageAccount.CreateCloudQueueClient();
        }

        public static CloudStorageAccount StorageAccount { get; set; }

        public static CloudQueueClient QueueClient { get; set; }

        static async Task Main()
        {
            var queues = await InitQueues();
            var builder = new HostBuilder();
            builder.ConfigureWebJobs(b =>
            {
                b.AddAzureStorageCoreServices();
                b.AddAzureStorage();
                b.AddTimers();
                b.AddCoubToYoutube();
            });
            
            builder.ConfigureLogging((context, b) => { b.AddConsole(); });
            var host = builder.Build();
            using (host)
            {
                await host.RunAsync();
            }
        }

        private static async Task<Dictionary<string, CloudQueue>> InitQueues()
        {
            var queuesToInit = new[]
            {
                QueueNames.Compile, 
                QueueNames.CoubImport,
                QueueNames.ScanSources
            };
            
            var result = new Dictionary<string, CloudQueue>(queuesToInit.Length);
            foreach (var queueName in queuesToInit)
            {
                var reference = QueueClient.GetQueueReference(queueName);
                await reference.CreateIfNotExistsAsync();
                result[queueName] = reference;
            }

            return result;
        }

        // private static void InitStorage()
        // {
        //     var blobContainerName = "coubs";
        //     _client = new BlobContainerClient(Settings.StorageAccount, blobContainerName);
        //     _client.CreateIfNotExists();
        // }
    }
}