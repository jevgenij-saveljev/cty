﻿using CoubDownloader.FileStorage;
using CoubDownloader.Messaging;
using CoubDownloader.Persistance;
using CoubDownloader.ScannerStrategies;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.DependencyInjection;

namespace CoubDownloader.WebJob.Extensions
{
    public static class CoubToYoutubeExtensions
    {
        public static IWebJobsBuilder AddCoubToYoutube(this IWebJobsBuilder builder)
        {
            builder.Services.AddTransient<CoubAppContext>((services) =>
            {
                var ctx = new CoubAppContext();
                return ctx;
            });
                
            builder.Services.AddTransient<CoubApiService>((services) =>
            {
                var ctx = new CoubApiService(SettingsProvider.CoubAccessToken);
                return ctx;
            });
                
            builder.Services.AddTransient<IFileStorage, AzureBlobStorageFileStorage>((services) =>
            {
                var storage = new AzureBlobStorageFileStorage(SettingsProvider.StorageAccount);
                return storage;
            });
                
            builder.Services.AddTransient<IScannerStrategy, LikesFeedStrategy>((services) =>
            {
                var likesFeedStrategy = new LikesFeedStrategy();
                return likesFeedStrategy;
            });
                
            builder.Services.AddTransient<IMessagingService, AzureStorageAccountQueueMessagingService>((services) =>
            {
                var messagingService = new AzureStorageAccountQueueMessagingService(SettingsProvider.StorageAccount);
                return messagingService;
            });

            return builder;
        }
    }
}