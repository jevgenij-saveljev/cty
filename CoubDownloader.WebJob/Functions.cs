﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CoubDownloader.FileStorage;
using CoubDownloader.Messaging;
using CoubDownloader.Messaging.DataTypes;
using CoubDownloader.Messaging.DataTypes.Messages;
using CoubDownloader.Models.V2;
using CoubDownloader.Persistance;
using CoubDownloader.ScannerStrategies;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Coub = CoubSharp.Model.Coub;

namespace CoubDownloader.WebJob
{
    public class Functions
    {
        private CoubAppContext _context;
        private CoubApiService _coubApiService;
        private IFileStorage _fileStorage;
        private IScannerStrategy _coubScanner;
        private IMessagingService _messagingService;


        public Functions(
            CoubAppContext ctx,
            CoubApiService coubApiService,
            IFileStorage fileStorage,
            IScannerStrategy coubScanner,
            IMessagingService messagingService)
        {
            this._context = ctx;
            this._coubApiService = coubApiService;
            this._fileStorage = fileStorage;
            this._coubScanner = coubScanner;
            this._messagingService = messagingService;
        }
        
        /// <summary>
        /// Downloads coubs from Coub.com into Storage (blob) 
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="logger">logger</param>
        /// <returns></returns>
        [Singleton]
        public async Task HandleImport([QueueTrigger(QueueNames.CoubImport)] ImportCoub message, ILogger logger)
        {
            logger.LogInformation("Handle Import Triggered");
            var downloadsPath = DirectoryUtility.GetUniqueOutputPath();
            var mergedPath = DirectoryUtility.GetUniqueOutputPath();

            var dict = new ConcurrentDictionary<string, Coub>();
            var linkTasks = message.Permalinks.Select(permalink => ToDownloadTask(permalink, dict));

            await Task.WhenAll(linkTasks.ToArray());
            
            var res = await this._coubApiService.DownloadCoubsAsync(dict.Values, downloadsPath);

            // TODO: Producer Consumer
            foreach (var downloadResult in res)
            {
                var merged = this._coubApiService.MergeCoubAudioAndVideo(downloadResult, mergedPath);
                var fName = Path.GetFileName(merged.Path);
                await this._fileStorage.UploadAsync(FileStoragePaths.CoubPath, fName, merged.Path);
                
                var existing = await this._context.Coubs
                    .FirstOrDefaultAsync(x => x.Permalink == merged.Coub.Permalink);
                
                var coub = new Models.V2.Coub()
                {
                    Duration = merged.Length,
                    Permalink = merged.Coub.Permalink,
                    CreatedAt = DateTime.UtcNow,
                    StorageProperties = new StorageProperties()
                    {
                        ContainerName = FileStoragePaths.CoubPath,
                        FileName = fName
                    }
                };

                if (existing is null)
                {
                    await this._context.AddAsync(coub);
                }
                else
                {
                    existing.Duration = coub.Duration;
                    existing.Permalink = coub.Permalink;
                    existing.CreatedAt = DateTime.Now;
                    existing.StorageProperties = new StorageProperties()
                    {
                        ContainerName = FileStoragePaths.CoubPath,
                        FileName = fName
                    };
                    // coub.Id = existing.Id;
                    // coub.
                    this._context.Update(coub);
                }                

                await this._context.SaveChangesAsync();
            }
            
            Directory.Delete(downloadsPath);
            Directory.Delete(mergedPath);
        }

        public async Task HandleScan([QueueTrigger(QueueNames.ScanSources)] string message, ILogger logger)
        {
            var newCoubs = await this._coubScanner.Scan();
            var importCoub = new ImportCoub()
            {
                Permalinks = newCoubs
            };
            
            var importCoubMessage = new ImportCoubMessage(importCoub);
            await this._messagingService.Dispatch(importCoubMessage);
        }        

        private async Task ToDownloadTask(string permalink, ConcurrentDictionary<string, Coub> dict)
        {
            var coub = await this._coubApiService.GetCoubAsync(permalink);
            dict.AddOrUpdate(permalink, coub, (key, oldValue) => oldValue);
        }
    }
}