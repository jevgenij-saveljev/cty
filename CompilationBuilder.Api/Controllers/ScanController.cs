﻿using System.Threading.Tasks;
using CoubDownloader.Messaging;
using CoubDownloader.Messaging.DataTypes.Messages;
using Microsoft.AspNetCore.Mvc;

namespace CompilationBuilder.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScanController : ControllerBase
    {
        private IMessagingService messagingService;

        public ScanController(IMessagingService messagingService)
        {
            this.messagingService = messagingService;
        }
        
        [HttpPost]
        public async Task<IActionResult> Post()
        {
            var scanMsg = new ScanMessage();
            await this.messagingService.Dispatch(scanMsg);

            return this.Ok();
        }
    }
}