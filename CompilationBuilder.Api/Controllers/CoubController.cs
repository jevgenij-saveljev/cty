﻿using System.Threading.Tasks;
using CoubDownloader.Messaging;
using CoubDownloader.Messaging.DataTypes;
using CoubDownloader.Messaging.DataTypes.Messages;
using Microsoft.AspNetCore.Mvc;

namespace CompilationBuilder.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CoubController : ControllerBase
    {
        private IMessagingService messagingService;

        public CoubController(IMessagingService messagingService)
        {
            this.messagingService = messagingService;
        }

        [HttpPost]
        [Route("import")]
        public async Task<IActionResult> Import([FromBody] ImportCoub importCoub)
        {
            var msg = new ImportCoubMessage(importCoub);
            await this.messagingService.Dispatch(msg);
            
            return this.Ok();
        }
    }
}