﻿using CoubDownloader.Models;
using CoubDownloader.Persistance;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CoubDownloader
{
    public class CompilationMetaService
    {
        public CompilationMetaService()
        {
            this.Context = new CoubAppContext();
        }

        public CoubAppContext Context { get; }

        public CoubCompilation GetLastCompilation()
        {
            var lastCoubCompilation = this.Context
                .CoubCompilations
                .Include(x => x.Coubs)
                .OrderBy(x => x.CreatedAt)
                .LastOrDefault();

            return lastCoubCompilation;
        }

        public CoubEntry GetLastCoubInCompilation(CoubCompilation coubCompilation)
        {
            var lastCoubInCompilation = coubCompilation
                    ?.Coubs
                    .OrderBy(x => x.Order)
                    .LastOrDefault();

            return lastCoubInCompilation;
        }
    }
}
