﻿using System;
using System.IO;

namespace CoubDownloader
{
    public class DirectoryUtility
    {
        public static string GetUniqueOutputPath()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var guid = Guid.NewGuid().ToString();
            var path = Path.Combine(currentDirectory, guid);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
    }
}