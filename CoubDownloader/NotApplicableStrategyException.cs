﻿using System;

namespace CoubDownloader
{
    public class NotApplicableStrategyException : Exception
    {
        public NotApplicableStrategyException(string message)
            : base(message)
        {

        }
    }
}
