﻿using CoubDownloader.Persistance;
using System;

namespace CoubDownloader
{
    public class ContinueFromLastSavedCoubStrategy : IStartingPointFinderStrategy
    {
        public ContinueFromLastSavedCoubStrategy(CoubAppContext context, CompilationMetaService compilationMetaService)
        {
            if (context is null)
            {
                throw new ArgumentException("Context is empty");
            }

            if (compilationMetaService is null)
            {
                throw new ArgumentException("CompilationMetaService is empty");
            }

            this.CompilationMetaService = compilationMetaService;
            this.Context = context;
        }

        private CompilationMetaService CompilationMetaService { get; }

        private CoubAppContext Context { get; }

        public string GetPermalink()
        {
            var lastCompilation = this.CompilationMetaService.GetLastCompilation();

            if (lastCompilation == null)
            {
                throw new NotApplicableStrategyException("can't find last compilation");
            }

            var lastCoubInCompilation = this.CompilationMetaService.GetLastCoubInCompilation(lastCompilation);

            if (lastCoubInCompilation is null)
            {
                System.Console.WriteLine($"Warning: missing coub permalink for compilation with id {0}", lastCompilation.Id);
                throw new NotApplicableStrategyException("can't find last coub in compilation");
            }

            var coubPermalink = lastCoubInCompilation.Link;

            return coubPermalink;
        }
    }
}
