﻿using CoubSharp.Model;
using System;
using System.Linq;

namespace CoubDownloader
{
    public class CoubDownloadRequest
    {
        public CoubFileMetadata Video { get; }
        public CoubFileMetadata Audio { get; }

        public CoubDownloadRequest(Coub coub)
        {
            this.Video = CoubFileMetadata.ForVideo(coub);
            this.Audio = CoubFileMetadata.ForAudio(coub);
        }
    }

    public struct CoubFileMetadata
    {
        public Uri Uri { get; set; }
        public string FileName { get; set; }

        public static CoubFileMetadata ForVideo(Coub coub)
        {

            var uri = CoubFileMetadata.GetVideoUri(coub);
            var fileName = uri.Segments.Last();
            return new CoubFileMetadata
            {
                FileName = fileName,
                Uri = uri
            };
        }

        public static CoubFileMetadata ForAudio(Coub coub)
        {

            var uri = CoubFileMetadata.GetAudioUri(coub);
            var fileName = uri.Segments.Last();
            return new CoubFileMetadata
            {
                FileName = fileName,
                Uri = uri
            };
        }

        private static Uri GetVideoUri(Coub coub)
        {
            var video = coub.FileVersions.Html5.Video;
            var urlVersion = video.High ?? video.Medium ?? video.Small;

            if (urlVersion is null)
            {
                // TODO: or throw?
                return null;
            }

            var uri = new Uri(urlVersion.Url);

            return uri;
        }

        private static Uri GetAudioUri(Coub coub)
        {
            var audio = coub.FileVersions.Html5.Audio;
            var urlVersion = audio.High ?? audio.Medium ?? audio.Small;

            if (urlVersion is null)
            {
                // TODO: or throw?
                return null;
            }

            var uri = new Uri(urlVersion.Url);

            return uri;
        }
    }
}
