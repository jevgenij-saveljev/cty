﻿using CoubDownloader.Models;
using CoubSharp.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;

namespace CoubDownloader
{
    public class CoubApiService
    {
        private static object locker = new object();
        public string AccessToken { get; }

        private HttpClient httpClient;

        public CoubApiService(string token)
        {
            this.AccessToken = token;
            this.httpClient = new HttpClient();
        }

        public async Task<CoubList> GetLikedCoubsAsync(int? page)
        {
            var ub = new UriBuilder("https://coub.com/api/v2/timeline/likes");
            var httpValueCollection = HttpUtility.ParseQueryString("");
            httpValueCollection.Add("access_token", this.AccessToken);

            if (page.HasValue)
            {
                httpValueCollection.Add("page", page.ToString());
            }

            ub.Query = httpValueCollection.ToString();

            var url = ub.ToString();

            var resp = await this.httpClient.GetAsync(url);
            var likedJson = await resp.Content.ReadAsStringAsync();
            var liked = Newtonsoft.Json.JsonConvert.DeserializeObject<CoubList>(likedJson);

            return liked;
        }

        public async Task<CoubList> AdvanceToPage(string permalink, int limit)
        {
            var page = 1;
            var total = 0;
            var containsSearchedCoub = false;
            var downloadedCount = 0;

            var liked = await this.GetLikedCoubsAsync(page);
            total = liked.total_pages;
            //downloadedCount += liked.coubs.Count();

            if (liked.coubs.Any(x => x.Permalink == permalink))
            {
                var coubIndex = liked.coubs.SkipWhile(x => x.Permalink != permalink).Count();
                var result = new CoubList
                {
                    page = liked.page,
                    total_pages = liked.total_pages,
                    coubs = liked.coubs.Skip(coubIndex)
                };
            }

            do
            {
                liked = await this.GetLikedCoubsAsync(page);
                page++;
                downloadedCount += liked.coubs.Count();
                containsSearchedCoub = liked.coubs.Any(x => x.Permalink == permalink);
            } while (page < total && !containsSearchedCoub && downloadedCount < limit);

            var _coubIndex = liked.coubs.SkipWhile(x => x.Permalink != permalink).Count();
            var _result = new CoubList
            {
                page = liked.page,
                total_pages = liked.total_pages,
                coubs = liked.coubs.Skip(_coubIndex)
            };

            return _result;
        }

        public async Task<DownloadResult> DownloadCoubAsync(Coub coub, string absoluteOutputDirectory)
        {
            if (!Directory.Exists(absoluteOutputDirectory))
            {
                throw new DirectoryNotFoundException(
                    $"prior downlaoding coub, output directory should created ${absoluteOutputDirectory}");
            }

            var videoWebClient = new WebClient();
            var coubDownloadRequest = new CoubDownloadRequest(coub);

            var audioWebClient = new WebClient();

            var audioPath = Path.Combine(absoluteOutputDirectory, coubDownloadRequest.Audio.FileName);
            var videoPath = Path.Combine(absoluteOutputDirectory, coubDownloadRequest.Video.FileName);

            var tasks = new Task[]
            {
                videoWebClient.DownloadFileTaskAsync(coubDownloadRequest.Video.Uri, videoPath),
                audioWebClient.DownloadFileTaskAsync(coubDownloadRequest.Audio.Uri, audioPath)
            };

            await Task.WhenAll(tasks);

            using (var fs = new FileStream(videoPath, FileMode.Open))
            {
                fs.Seek(0, SeekOrigin.Begin);
                fs.WriteByte(0);
                fs.Seek(1, SeekOrigin.Begin);
                fs.WriteByte(0);
            }

            var pillarBoxedFileName = $"_{coubDownloadRequest.Video.FileName}";
            var pillarBoxedVideoPath = Path.Combine(absoluteOutputDirectory, pillarBoxedFileName);

            // add pillar boxes to video (helps to avoid auto-scale)
            // ffmpeg -i "muted_mp4_big_size_1577679459_muted_big.mp4" -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2" 1.mp4

            // Console.WriteLine("list directories");
            // var files = Directory.GetFiles(@"C://ffmpeg/bin");
            // foreach (var file in files)
            // {
            //     Console.WriteLine(file);
            // }

            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = SettingsProvider.FfmpegPath;

            // var path = Path.Combine(outputFolder, downloadResult.Coub.Permalink + ".mp4");

            // ffmpeg 

            proc.Arguments =
                $@"-i ""{videoPath}"" -y -vf ""scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2"" ""{pillarBoxedVideoPath}""";
            // proc.RedirectStandardOutput = true;
            // proc.RedirectStandardError = true;

            lock (locker)
            {
                var process = Process.Start(proc);
                process.OutputDataReceived += (sender, args) =>
                {
                    var a = 1;
                };

                process.ErrorDataReceived += (sender, args) =>
                {
                    var b = 1;
                };

                process.WaitForExit();
                // var so = process.StandardError.ReadToEnd();
            }


            var result = new DownloadResult(coub, pillarBoxedVideoPath, audioPath);

            return result;
        }

        public async Task<Coub> GetCoubAsync(string permalink)
        {
            var response = await this.httpClient
                .GetAsync($"https://coub.com/api/v2/coubs/{permalink}?access_token={this.AccessToken}");

            var body = await response.Content.ReadAsStringAsync();
            var coub = JsonConvert.DeserializeObject<Coub>(body);

            return coub;
        }

        public async Task<IEnumerable<DownloadResult>> DownloadCoubsAsync(IEnumerable<Coub> coubs, string outputFolder)
        {
            var outputDirectory = string.Empty;
            if (Directory.Exists(outputFolder))
            {
                // it's absolute path
                // but passign here relative directory here will anyway stil be valid
                outputDirectory = outputFolder;
            }
            else
            {
                // it's relative
                outputDirectory = Path.GetRelativePath(Directory.GetCurrentDirectory(), outputFolder);
            }

            outputDirectory = Path.GetFullPath(outputDirectory);

            lock (locker)
            {
                if (!Directory.Exists(outputDirectory))
                {
                    Directory.CreateDirectory(outputDirectory);
                }
            }

            var tasks = new List<Task<DownloadResult>>(coubs.Count());
            foreach (var coub in coubs)
            {
                var task = this.DownloadCoubAsync(coub, outputDirectory);
                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            var results = tasks
                .Select(x => x.Result)
                .ToList();

            return results;
        }

        public FileSystemCoub MergeCoubAudioAndVideo(DownloadResult downloadResult, string outputFolder)
        {
            // if (outp)
            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = SettingsProvider.FfmpegPath;

            var path = Path.Combine(outputFolder, downloadResult.Coub.Permalink + ".mp4");

            // ffmpeg - i video.mp4 - i audio.wav - c copy output.mkv
            // ffmpeg -i input -filter_complex "[0:v]scale=iw/2:-1[v]" -map "[v]" -map 0:a -c:a copy output


            // ffmpeg -i "muted_mp4_big_size_1577679459_muted_big.mp4" -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:(ow-iw)/2:(oh-ih)/2" 1.mp4


            proc.Arguments =
                $@"-i {downloadResult.VideoPath} -i {downloadResult.AudioPath} -shortest -y -c copy {path}";
            // proc.Arguments = $@"-i {downloadResult.VideoPath} -i {downloadResult.AudioPath} -filter_complex ""[0:v]scale=iw/2:-1[v]"" -map ""[v]"" -map 0:a -c:a copy o -shortest -y -c copy {path}";
            proc.RedirectStandardOutput = true;
            proc.RedirectStandardError = true;
            var process = Process.Start(proc);

            process.WaitForExit();
            //var output = process.StandardOutput;
            //var outputText = output.ReadToEnd();
            var outputError = process.StandardError;
            var outputErrorText = outputError.ReadToEnd();

            Console.WriteLine($"Merge Audio task (ffmpeg) has exited with: {process.HasExited}");
            Console.WriteLine("Output Text");
            Console.WriteLine(outputErrorText);
            // var idx = outputErrorText.IndexOf("time=");
            // var timeStr = outputErrorText.Substring("time=".Length + idx, 11);


            var timeParserProcessStartnInfo = new ProcessStartInfo();
            // timeParserProcessStartnInfo.FileName = @"C://ffmpeg/bin/ffprobe";
            timeParserProcessStartnInfo.FileName = SettingsProvider.FfprobePath;
            timeParserProcessStartnInfo.Arguments = $@"-i {path} -show_format -sexagesimal";


            // timeParserProcessStartnInfo.RedirectStandardOutput = false;
            // timeParserProcessStartnInfo.RedirectStandardError = false;
            timeParserProcessStartnInfo.RedirectStandardOutput = true;
            timeParserProcessStartnInfo.RedirectStandardError = true;


            var timeParserProcess = new Process();
            timeParserProcess.StartInfo = timeParserProcessStartnInfo;
            // timeParserProcess.Start();
            // var timeParserProcess = Process.Start(timeParserProcessStartnInfo);
            var text = string.Empty;
            timeParserProcess.ErrorDataReceived += (arg1, arg2) =>
            {
                Console.WriteLine(arg2.Data);
                text += arg2.Data;
            };
            
            timeParserProcess.OutputDataReceived+= (arg1, arg2) =>
            {
                Console.WriteLine(arg2.Data);
                text += arg2.Data;
            };

            timeParserProcess.Exited += (sender, args) => { Console.WriteLine("Proccess exited"); };

            timeParserProcess.Start();
            timeParserProcess.WaitForExit();
            Console.WriteLine($"exit code {timeParserProcess.ExitCode}");
            // var txtE = timeParserProcess.StandardError.ReadToEndAsync().GetAwaiter().GetResult();
            var txtM = timeParserProcess.StandardOutput.ReadToEndAsync().GetAwaiter().GetResult();

            // var ts = TimeSpan.Parse(timeStr);
            var timeStrr = txtM;
            Console.WriteLine(timeStrr);
            var idxx = timeStrr.IndexOf("duration=");
            var timeStrs = string.Empty;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                timeStrs = timeStrr.Substring("duration:".Length + idxx, 10);
            }
            else
            {
                timeStrs = timeStrr.Substring("duration=".Length + idxx, 10);
            }

            var ts2 = TimeSpan.Parse(timeStrs);

            var result = new FileSystemCoub
            {
                Coub = downloadResult.Coub,
                Path = path,
                Length = ts2
            };

            return result;
        }

        public void CreateCompilation(IEnumerable<string> paths, string outputFolder)
        {
            var joinFilePath = Path.Combine(outputFolder, "join.txt");
            using (var fs = new FileStream(joinFilePath, FileMode.CreateNew))
            using (var sr = new StreamWriter(fs))
            {
                foreach (var filePath in paths)
                {
                    sr.WriteLine($"file {Path.GetRelativePath(outputFolder, filePath)}");
                }
            }

            ProcessStartInfo proc = new ProcessStartInfo();
            proc.FileName = SettingsProvider.FfmpegPath;
            proc.RedirectStandardError = false;
            proc.WorkingDirectory = outputFolder;

            // ffmpeg -f concat -i join.txt.txt -c copy yaya.mp4
            proc.Arguments = $@"-f concat -i join.txt -y -c copy compilation.mp4";
            var process = Process.Start(proc);

            process.WaitForExit();
            //StreamReader myStreamReader = process.StandardError;
            // Read the standard error of net.exe and write it on to console.
            //var err = myStreamReader.ReadToEnd();
            //Console.WriteLine(myStreamReader.ReadLine());
        }
    }
}