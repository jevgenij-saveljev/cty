﻿using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace CoubDownloader.FileStorage
{
    public class AzureBlobStorageFileStorage : IFileStorage
    {
        private CloudStorageAccount _storageAccount;
        private CloudBlobClient _blobClient;

        public AzureBlobStorageFileStorage(string connectionString)
        {
            this._storageAccount = CloudStorageAccount.Parse(connectionString);
            this._blobClient = this._storageAccount.CreateCloudBlobClient();
        }

        public async Task UploadAsync(string container, string fileName, string path)
        {
            var containerRef = this._blobClient.GetContainerReference(container);
            await containerRef.CreateIfNotExistsAsync();
            var blobRef = containerRef.GetBlockBlobReference(fileName);
            await blobRef.UploadFromFileAsync(path);
        }
    }
}