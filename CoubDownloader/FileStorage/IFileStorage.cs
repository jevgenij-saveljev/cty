﻿using System.Threading.Tasks;

namespace CoubDownloader.FileStorage
{
    public interface IFileStorage
    {
        Task UploadAsync(string container, string fileName, string path);
    }
}