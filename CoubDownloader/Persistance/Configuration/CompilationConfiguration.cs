﻿using CoubDownloader.Models;
using CoubDownloader.Models.V2;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoubDownloader.Persistance.Configuration
{
    public class CompilationConfiguration : IEntityTypeConfiguration<Compilation>
    {
        public void Configure(EntityTypeBuilder<Compilation> builder)
        {
            builder.ToTable("Compilations", "coub_downloader");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.CreatedAt);

            builder.Property(x => x.Duration);
            // builder.OwnsOne(x => x.StorageProperties, navigationBuilder =>
            // {
            //     navigationBuilder.WithOwner();
            //
            //     // navigationBuilder.Has
            //     navigationBuilder.Property(x => x.ContainerName)
            //         .HasColumnName(nameof(StorageProperties.ContainerName));
            //
            //     navigationBuilder.Property(x => x.FileName)
            //         .HasColumnName(nameof(StorageProperties.FileName));
            // });

            builder.OwnsOne<StorageProperties>(x => x.StorageProperties);
            // builder.OwnsOne(x => x.StorageProperties);

            builder.Property(x => x.Status);
        }
    }
}