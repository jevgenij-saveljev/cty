﻿using CoubDownloader.Models;
using CoubDownloader.Models.V2;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CoubDownloader.Persistance.Configuration
{
    public class CompilationCoubConfiguration :  IEntityTypeConfiguration<CompilationCoub>
    {
        public void Configure(EntityTypeBuilder<CompilationCoub> builder)
        {
            builder.ToTable("CompilationCoubs", "coub_downloader");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.Order);
            builder.HasOne(x => x.Compilation)
                .WithMany()
                .HasForeignKey(x => x.CompilationId);
            
            builder.HasOne(x => x.Coub)
                .WithMany()
                .HasForeignKey(x => x.CoubId);
        }
    }
}