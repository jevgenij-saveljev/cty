﻿using CoubDownloader.Models;
using Microsoft.EntityFrameworkCore;

namespace CoubDownloader.Persistance.Configuration
{
    public class CoubEntryConfiguration : IEntityTypeConfiguration<CoubEntry>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CoubEntry> builder)
        {
            builder.ToTable("CoubEntry", "coub_downloader");

            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.CoubCompilation);
            builder.Property(x => x.Link);
            builder.Property(x => x.Name);
        }
    }
}
