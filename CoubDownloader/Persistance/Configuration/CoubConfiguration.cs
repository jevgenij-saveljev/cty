﻿using CoubDownloader.Models.V2;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace CoubDownloader.Persistance.Configuration
{
    public class CoubConfiguration : IEntityTypeConfiguration<Coub>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Coub> builder)
        {
            var timeSpanToTicksConverter = new TimeSpanToTicksConverter();
            builder.ToTable("Coub", "coub_downloader");

            builder.HasKey(x => x.Id);
            builder.Property(x => x.StorageLink);
            builder.Property(x => x.CreatedAt);
            builder.Property(x => x.Permalink);
            builder.Property(x => x.Duration)
                .HasConversion(timeSpanToTicksConverter);


            builder.OwnsOne(x => x.StorageProperties, navigationBuilder =>
            {
                navigationBuilder.WithOwner();

                // navigationBuilder.Has
                navigationBuilder.Property(x => x.ContainerName)
                    .HasColumnName(nameof(StorageProperties.ContainerName));

                navigationBuilder.Property(x => x.FileName)
                    .HasColumnName(nameof(StorageProperties.FileName));
            });
        }
    }
}