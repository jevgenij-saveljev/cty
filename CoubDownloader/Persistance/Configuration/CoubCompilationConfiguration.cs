﻿using CoubDownloader.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
//using Microsoft.EntityFrameworkCore.

namespace CoubDownloader.Persistance.Configuration
{
    public class CoubCompilationConfiguration : IEntityTypeConfiguration<CoubCompilation>
    {
        public void Configure(EntityTypeBuilder<CoubCompilation> builder)
        {
            builder.ToTable("CoubCompilations", "coub_downloader");

            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Coubs)
                .WithOne(x => x.CoubCompilation);
            builder.Property(x => x.CreatedAt);

        }
    }
}
