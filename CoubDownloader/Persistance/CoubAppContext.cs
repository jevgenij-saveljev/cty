﻿using CoubDownloader.Models;
using CoubDownloader.Models.V2;
using Microsoft.EntityFrameworkCore;

namespace CoubDownloader.Persistance
{
    public class CoubAppContext : DbContext
    {
        public DbSet<CoubCompilation> CoubCompilations { get; set; }
        public DbSet<CoubEntry> CoubEntries { get; set; }
        public DbSet<Coub> Coubs { get; set; }
        public DbSet<Compilation> Compilations { get; set; }
        public DbSet<CompilationCoub> CompilationCoubs { get; set; }

        private string ConnectionString { get; }

        public CoubAppContext()
        {
            this.ConnectionString = SettingsProvider.DbConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(this.ConnectionString);
        }
    }
}
