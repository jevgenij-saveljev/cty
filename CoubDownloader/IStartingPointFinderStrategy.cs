﻿namespace CoubDownloader
{
    interface IStartingPointFinderStrategy
    {
        string GetPermalink();
    }
}
