﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using CoubDownloader.Persistance;
using Microsoft.Data.SqlClient;

namespace CoubDownloader.ScannerStrategies
{
    public class LikesFeedStrategy : IScannerStrategy
    {
        private CoubApiService _coubApiService;
        private CoubAppContext _coubAppContext;

        public LikesFeedStrategy()
        {
            this.SetApiService();
            this.SetContext();
        }

        public async Task<IEnumerable<string>> Scan()
        {
            var coubsToProcess = new List<string>();
            var page = 0;
            var pageSize = 10;
            var coubsReadedFromDb = -1;
            var coubsReadedFromApi = -1;
            var totalPages = -1;
            do
            {
                page++;
                var coubPage = await this._coubApiService
                    .GetLikedCoubsAsync(page);
                totalPages = coubPage.total_pages;
                // TODO: compose except query 
                var links = coubPage.coubs.Select(x => x.Permalink);

                var coubBatch = this.GetNewCoubs(links);
                var coubBatchList = coubBatch as string[] ?? coubBatch.ToArray();
                coubsToProcess.AddRange(coubBatchList);
                coubsReadedFromApi = coubPage.coubs.Count();
                coubsReadedFromDb = coubBatchList.Count();
            } while (coubsReadedFromDb == coubsReadedFromApi && page <= totalPages);

            return coubsToProcess;


            // // TODO: send to Azure Container Instances 
            // string rgName = SdkContext.RandomResourceName("rgACI", 15);
            // string aciName = SdkContext.RandomResourceName("acisample", 20);
            // string shareName = SdkContext.RandomResourceName("fileshare", 20);
            // string containerImageName = "seanmckenna/aci-hellofiles";
            // string volumeMountName = "aci-helloshare";
            //
            // AzureCredentials credentials = SdkContext.AzureCredentialsFactory.FromFile(Environment.GetEnvironmentVariable("AZURE_AUTH_LOCATION"));
            //
            // var azure = Azure
            //     .Configure()
            //     .WithLogLevel(HttpLoggingDelegatingHandler.Level.Basic)
            //     .Authenticate(credentials)
            //     .WithDefaultSubscription();
            //
            // IContainerGroup containerGroup = azure.ContainerGroups.Define(aciName)
            //     .WithRegion(region)
            //     .WithNewResourceGroup(rgName)
            //     .WithLinux()
            //     .WithPublicImageRegistryOnly()
            //     .WithNewAzureFileShareVolume(volumeMountName, shareName)
            //     .DefineContainerInstance(aciName)
            //     .WithImage(containerImageName)
            //     .WithExternalTcpPort(80)
            //     .WithVolumeMountSetting(volumeMountName, "/aci/logs/")
            //     .Attach()
            //     .WithDnsPrefix(aciName)
            //     .Create();
        }

        private IEnumerable<string> GetNewCoubs(IEnumerable<string> permalinks)
        {
            var result = new List<string>();
            var conString = SettingsProvider.DbConnectionString;
            var command = new SqlCommand();


            var dt = new DataTable();
            dt.Columns.Add("permalink", typeof(string));
            foreach (var link in permalinks)
            {
                dt.Rows.Add(link);
            }

            var changes = dt.GetChanges(DataRowState.Added);
            SqlParameter arrayParameter = command.Parameters.AddWithValue("@array", dt);
            arrayParameter.SqlDbType = SqlDbType.Structured;
            arrayParameter.TypeName = "dbo.OneDimensionalVarCharArray";

            command.CommandText = @"select * from @array
except 
select permalink from dbo.Coubs
";


            using (SqlConnection connection = new SqlConnection(conString))
            {
                connection.Open();
                command.Connection = connection;
                using (command)
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                var permalink = reader.GetString(i);
                                Console.WriteLine("New entry to process {0}", permalink);
                                result.Add(permalink);
                            }
                        }
                    }
                }
            }

            return result;
        }

        private void SetApiService()
        {
            var token = SettingsProvider.CoubAccessToken;
            this._coubApiService = new CoubApiService(token);
        }

        private void SetContext()
        {
            var ctx = new CoubAppContext();
            this._coubAppContext = ctx;
        }
    }
}