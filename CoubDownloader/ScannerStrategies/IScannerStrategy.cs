﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoubDownloader.ScannerStrategies
{
    public interface IScannerStrategy
    {
        Task<IEnumerable<string>> Scan();
    }
}