﻿namespace CoubDownloader.Messaging
{
    public static class QueueNames
    {
        public const string CoubImport = "coub-import";
        
        public const string ScanSources = "scan-sources";
        
        public const string Compile = "compile";
    }
}