﻿using System.Threading.Tasks;
using CoubDownloader.Messaging.DataTypes;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace CoubDownloader.Messaging
{
    public class AzureStorageAccountQueueMessagingService : IMessagingService
    {
        private CloudStorageAccount _storageAccount;
        private CloudQueueClient _cloudQueueClient;

        public AzureStorageAccountQueueMessagingService(string connectionString)
        {
            this._storageAccount = CloudStorageAccount.Parse(connectionString);
            this._cloudQueueClient = this._storageAccount.CreateCloudQueueClient();
        }

        public async Task Dispatch<T>(Message<T> message)
            where T : class, IDataType
        {
            var queueRef = this._cloudQueueClient
                .GetQueueReference(message.Destination);

            // TODO: Create queue, only if send fails with no such queue Exception
            await queueRef.CreateIfNotExistsAsync();

            string payload = null;
            if (message.Payload != null)
            {
                payload = JsonConvert.SerializeObject(message.Payload);
            }
           
            var msg = new CloudQueueMessage(payload);

            await queueRef.AddMessageAsync(msg);
        }
    }
}