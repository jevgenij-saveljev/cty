﻿using System.Threading.Tasks;
using CoubDownloader.Messaging.DataTypes;

namespace CoubDownloader.Messaging
{
    public interface IMessagingService
    {
        Task Dispatch<T>(Message<T> message) where T : class, IDataType;
    }
}