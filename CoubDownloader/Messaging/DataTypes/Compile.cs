﻿namespace CoubDownloader.Messaging.DataTypes
{
    public class Compile : IDataType
    {
        public long CompilationId { get; set; }
    }
}