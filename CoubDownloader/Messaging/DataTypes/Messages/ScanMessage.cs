﻿namespace CoubDownloader.Messaging.DataTypes.Messages
{
    public class ScanMessage : Message<ImportCoub>
    {
        public ScanMessage()
            : base(QueueNames.ScanSources)
        {
            
        }        
    }
}