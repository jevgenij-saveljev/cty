﻿namespace CoubDownloader.Messaging.DataTypes.Messages
{
    public class ImportCoubMessage : Message<ImportCoub>
    {
        public ImportCoubMessage(ImportCoub payload)
            : base(QueueNames.CoubImport, payload)
        {
            
        }
    }
}