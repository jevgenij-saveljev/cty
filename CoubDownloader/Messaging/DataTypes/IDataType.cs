﻿namespace CoubDownloader.Messaging.DataTypes
{
    /// <summary>
    /// Marker Interface to avoid Generic generation of messaging service for every type
    /// </summary>
    public interface IDataType
    {
        
    }
}