﻿using System.Collections.Generic;

namespace CoubDownloader.Messaging.DataTypes
{
    public class ImportCoub : IDataType
    {
        public IEnumerable<string> Permalinks { get; set; }
    }
}