﻿namespace CoubDownloader.Messaging.DataTypes
{
    public class Message<T> where T : class, IDataType
    {
        public Message(string destination, T payload)
        {
            this.Destination = destination;
            this.Payload = payload;
        }

        public Message(string destination)
        {
            this.Destination = destination;
            this.Payload = null;
        }

        public string Destination { get; }

        public T Payload { get; }
    }
}