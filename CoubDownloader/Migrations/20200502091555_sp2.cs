﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoubDownloader.Migrations
{
    public partial class sp2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StorageProperties_ContainerName",
                table: "Coubs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StorageProperties_FileName",
                table: "Coubs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StorageProperties_ContainerName",
                table: "Coubs");

            migrationBuilder.DropColumn(
                name: "StorageProperties_FileName",
                table: "Coubs");
        }
    }
}
