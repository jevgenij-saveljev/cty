﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CoubDownloader.Migrations
{
    public partial class V2_Entities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Compilations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Duration = table.Column<TimeSpan>(nullable: false),
                    StorageProperties_ContainerName = table.Column<string>(nullable: true),
                    StorageProperties_FileName = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Compilations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CompilationCoubs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CoubId = table.Column<long>(nullable: false),
                    CompilationId = table.Column<long>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompilationCoubs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompilationCoubs_Compilations_CompilationId",
                        column: x => x.CompilationId,
                        principalTable: "Compilations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompilationCoubs_Coubs_CoubId",
                        column: x => x.CoubId,
                        principalTable: "Coubs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompilationCoubs_CompilationId",
                table: "CompilationCoubs",
                column: "CompilationId");

            migrationBuilder.CreateIndex(
                name: "IX_CompilationCoubs_CoubId",
                table: "CompilationCoubs",
                column: "CoubId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompilationCoubs");

            migrationBuilder.DropTable(
                name: "Compilations");
        }
    }
}
