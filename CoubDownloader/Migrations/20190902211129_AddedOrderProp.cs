﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CoubDownloader.Migrations
{
    public partial class AddedOrderProp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "CoubEntries",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "CoubEntries");
        }
    }
}
