﻿using CommandLine;
using CoubDownloader.Models;
using CoubDownloader.Persistance;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoubDownloader.Console
{
    class Program
    {
        private static int _coubCountInCompilation = 70;

        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<GenerateOptions>(args)
            .WithParsed<GenerateOptions>(opts => GenerateAsync(opts).Wait())
            .WithNotParsed<GenerateOptions>((errs) =>
            {
                foreach (var err in errs)
                {
                    System.Console.WriteLine(err.Tag);
                }
            });
        }

        private static async Task GenerateAsync(GenerateOptions opts)
        {
            var compilationMetaService = new CompilationMetaService();

            var ctx = new CoubAppContext();
            var lastCompilation = compilationMetaService
                .GetLastCompilation();

            // get Permalink of latest coub if exists
            string coubPermalink = null;

            if (lastCompilation != null)
            {
                var lastCoubInCompilation = compilationMetaService.GetLastCoubInCompilation(lastCompilation);

                if (lastCoubInCompilation is null)
                {
                    System.Console.WriteLine($"Warning: missing coub permalink for compilation with id {0}", lastCompilation.Id);
                }

                coubPermalink = lastCoubInCompilation.Link;
            }

            var accesToken = "620d83c6977620f18eabd02e51f6da87c786283cc5c694ab668f7b32a57bccc3";
            var coubSvc = new CoubApiService(accesToken);

            CoubList liked = null;
            var page = 1;
            var total = 0;
            var downloaded = 0;
            var paths = new List<string>(Program._coubCountInCompilation);
            var fileSystemCoubs = new List<FileSystemCoub>(Program._coubCountInCompilation);

            string folderName = GetFolderName();

            //var initialCoubList = await coubSvc.GetCoubPageNumberAsync(coubPermalink, Program._coubCountInCompilation);
            do
            {
                if (page == 1)
                {
                    liked = await coubSvc.AdvanceToPage(coubPermalink, Program._coubCountInCompilation);
                }
                else
                {
                    liked = await coubSvc.GetLikedCoubsAsync(--page);
                }

                page = liked.page;
                total = liked.total_pages;

                var downloads = await coubSvc.DownloadCoubsAsync(liked.coubs, folderName);
                downloaded += downloads.Count();

                foreach (var download in downloads)
                {
                    var result = coubSvc.MergeCoubAudioAndVideo(download, folderName);
                    fileSystemCoubs.Add(result);
                    paths.Add(result.Path);
                }
            } while (page > 1 && downloaded <= Program._coubCountInCompilation);

            paths = paths.Distinct().ToList();
            
            var thumbnailCoub = await coubSvc
                .GetCoubAsync(opts.ThumbnailPermalink);

            var thumbnailDownloadResult = await coubSvc.DownloadCoubAsync(thumbnailCoub, folderName);
            var videoResult = coubSvc.MergeCoubAudioAndVideo(thumbnailDownloadResult, folderName);
            fileSystemCoubs.Add(videoResult);
            paths.Add(videoResult.Path);

            coubSvc.CreateCompilation(paths, folderName);

            var sb = new StringBuilder();
            //foreach (var item in fileSystemCoubs)
            //{
            //    sb.AppendLine($"{item.Coub.Title} {}")
            //}

            for (int i = 0; i < fileSystemCoubs.Count; i++)
            {
                var item = fileSystemCoubs[i];
                var timeSum = fileSystemCoubs
                    .TakeWhile(x => x != item)
                    .Select(x => x.Length)
                    .Aggregate(TimeSpan.FromSeconds(0), (sum, next) =>
                    {
                        //var ts = TimeSpan.FromSeconds(next.Seconds);
                        return sum + next;
                    });

                sb.AppendLine($"{item.Coub.Title} {timeSum.ToString(@"mm\:ss")} https://coub.com/view/{item.Coub.Permalink}");
            }

            byte[] byteArray = Encoding.ASCII.GetBytes(sb.ToString());
            File.WriteAllText(Path.Combine(folderName, "descritption.txt"), sb.ToString(), Encoding.UTF8);

            var thumbnailWebClient = new WebClient();
            await thumbnailWebClient.DownloadFileTaskAsync(thumbnailCoub.ImageVersions.Template.Replace("%{version}", "big"), Path.Combine(folderName, "thumbnail.jpg"));

            var compilation = new CoubCompilation
            {
                CreatedAt = DateTime.UtcNow,
                Coubs = new List<CoubEntry>()
            };

            for (int i = 0; i < fileSystemCoubs.Count; i++)
            {
                var item = fileSystemCoubs[i];
                var entry = new CoubEntry
                {
                    CoubCompilation = compilation,
                    Link = item.Coub.Permalink,
                    Name = item.Coub.Title,
                    Order = i + 1
                };

                compilation.Coubs.Add(entry);
            }

            ctx.Add(compilation);
            await ctx.SaveChangesAsync();

            //using (var fs = File.Open(, FileMode.CreateNew))
            //using (var sw = new StreamWriter(fs))
            //using (var ms = new MemoryStream(byteArray))
            //using (var sr = new StreamReader(ms))
            //{
            //    var readed = 
            //}

            // ffmpeg -f concat -i join.txt.txt -c copy yaya.mp4


            // https://www.newtonsoft.com/json/help/html/Performance.htm

            //ICoubService coubService = new CoubService(accesToken);



            //IAuthorizationService authorizationService = new AuthorizationService("72ad0e54cfbe648f9394a3f07979922c81b52933e77f3c30fb4c50990026b1f2", "c2d586143cd3bc41da662b54415a8636c2f7cd897104a22d08a48b8339eaa832");
            ////Now we can generate URL to get authorize code
            ////It comes to redirect URL with additional parameter http://yourWebSite.domain/authorize/?code=0x0x0x0x0
            //var urlToGetAuthorizeCode = authorizationService.AuthorizationCodeUrlAsync("https://goal-manager.azurewebsites.net", new string[] { AuthorizationService.Scope.LoggedIn, AuthorizationService.Scope.Recoub, AuthorizationService.Scope.Create, AuthorizationService.Scope.ChannelEdit, AuthorizationService.Scope.Follow });
            //System.Console.WriteLine("Should Open that link in browser and accept it");
            //System.Console.WriteLine(urlToGetAuthorizeCode);
            //System.Console.WriteLine();
            //System.Console.WriteLine();
            //System.Console.WriteLine();

            //var code = System.Console.ReadLine();
            //var idx = code.IndexOf("code=");
            //code = code.Substring(idx + "code=".Length);


            //// https://goal-manager.azurewebsites.net/?code=0567885a61e1a9bf0229867e1aadca2089e57ee081a3e030fb83d42285f1a198
            ////With code we can get an access token
            //var token = await authorizationService.AuthorizeTokenAsync("https://goal-manager.azurewebsites.net", "0567885a61e1a9bf0229867e1aadca2089e57ee081a3e030fb83d42285f1a198");

            //System.Console.WriteLine(token);

            //var coub = await coubService.Coubs.GetCoubAsync("1vqq2k");
            //coubSvc.DownloadAsync()


            //var videoWebClient = new WebClient();
            //var coubDownloadRequest = new CoubDownloadRequest(coub);

            //var audioWebClient = new WebClient();

            //var tasks = new Task[]
            //{
            //    videoWebClient.DownloadFileTaskAsync(coubDownloadRequest.Video.Uri, coubDownloadRequest.Video.FileName),
            //    audioWebClient.DownloadFileTaskAsync(coubDownloadRequest.Audio.Uri, coubDownloadRequest.Audio.FileName)
            //};

            //await Task.WhenAll(tasks);

            //using (var fs = new FileStream(coubDownloadRequest.Video.FileName, FileMode.Open))
            //{
            //    fs.Seek(0, SeekOrigin.Begin);
            //    fs.WriteByte(0);
            //    fs.Seek(1, SeekOrigin.Begin);
            //    fs.WriteByte(0);
            //}

        }

        private static string GetFolderName()
        {
            return DateTime.Now.ToString()
                .Replace("/", "-")
                .Replace(" ", "__")
                .Replace(":", "_");
        }
    }
}
