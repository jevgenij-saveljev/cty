﻿using CommandLine;

namespace CoubDownloader.Console
{
    [Verb("generate", HelpText = "Generates coub compilation")]
    public class GenerateOptions
    {
        [Option('c', "count", Required = true, HelpText = "last N coubs to take in compilation.")]
        public int CoubCount { get; set; }

        [Option('t', "thumbnail", Required = true, HelpText = "Thumbnail permalink.")]
        public string ThumbnailPermalink { get; set; }

        //[Value(1, MetaName = "count", HelpText = "last N coubs to take in compilation. Default  70", Required = true)]
        //public long? Count { get; set; }

        //[Value(2, MetaName = "thumbnail", HelpText = "thumbnaiil", Required = true)]
        //public string ThumbnailUrl { get; set; }
    }
}
